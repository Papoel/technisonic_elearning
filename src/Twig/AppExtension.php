<?php

namespace App\Twig;

use Twig\TwigFilter;
use App\Entity\Cours;
use Twig\TwigFunction;
use Twig\Extra\Intl\IntlExtension;
use Twig\Extension\AbstractExtension;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('datetime', [$this, 'formatDateTime']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'pluralize']),
        ];
    }

    public function pluralize(int $count, string $singular, ?string $plural = null): string
    {
        $plural ??= $singular . 's';

        $string = $count === 1 ? $singular : $plural;

        return "$count $string";
    }


    public function formatDateTime(\DateTimeInterface $dateTime): string
    {
        date_default_timezone_set('Europe/Paris');
        setlocale(LC_ALL, 'fra_fra');
        // return $dateTime->format('F d, Y \a\t h:i A');
        
        $heure = $dateTime->format('H:i');

        // samedi 21 juin 2021 => samedi 21 juin
        // $dateTime = utf8_encode(strftime("%A %d %B %Y"));
        // 21-06-2021
        $dateTime = utf8_encode(strftime("%d-%m-%Y"));
        return $dateTime .' à '. $heure;
    }
}
