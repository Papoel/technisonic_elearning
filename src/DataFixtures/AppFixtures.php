<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Cours;
use App\Entity\Salarie;
use App\Entity\Categorie;
use App\Entity\Technique;
use App\Entity\Procedures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        // - Initialisation de Faker pour obtenir des données Fr
        $faker = Faker\Factory::create('fr_FR');

        // - création de 20 salaries
        for ($i = 1; $i <= 20; $i++) {
            $salaries = [];
            $salarie = new Salarie();
            $salarie->setNom($faker->lastName());
            $salarie->setPrenom($faker->firstName());
            $salarie->setNni(strtoupper($faker->bothify('??##?#??')));
            $salarie->setEmail('user' . $i . '@technisonic.fr');
            $salarie->setPassword('password');
            $salarie->setRoles("['ROLE_USER']");
            $salarie->setAgence($faker->randomElement(['Dunkerque', 'Terville']));
            $salarie->setIsVerified(true);
            $salarie->setIsFormateur($faker->boolean());
            $salarie->setIsAdmin($faker->boolean());
            $salarie->setIsSuperviseur($faker->boolean());
            
            $manager->persist($salarie);
            $salaries[] = $salarie;
        }

        // - Création des techniques 
        for ($i = 1; $i <= 4; $i++) {
            $technique = new Technique();
            $value = ['ressuage', 'magnetoscopie', 'ultrason', 'radiographie'];
            $technique->setName($value[$i - 1]);

            $manager->persist($technique);
        }

        // - Génération de procédures
        for ($i = 1; $i <= 20; $i++) {
            $procedure = new Procedures();
            $procedure->setNom(sprintf("CC.P/0%d", $i));
            $procedure->setRdu($faker->bothify('D##############'));
            $procedure->setIndice($faker->numberBetween(0, 12));
            $procedure->setIsQualifie($faker->boolean());

            $manager->persist($procedure);
        }

        // - Génération des catégories
        for ($i = 1; $i <= 6; $i++) {
            $categories = [];
            $categorie = new Categorie();
            $value = ['ressuage', 'magnetoscopie', 'ultrason', 'radiographie', 'securite', 'general'];
            $categorie->setNom($value[$i - 1]);
            
            $manager->persist($categorie);
            $categories[] = $categorie;
        }

        // - Génération de cours
        for ($i = 1; $i <= 20; $i++) {
            $cours = new Cours();
            $cours->setTitre($faker->text(30));
            $cours->setContenu($faker->text($faker->numberBetween(100, 3500)));
            $cours->setCategorie($faker->randomElement($categories));
            $cours->setFormateur($faker->randomElement($salaries));

            $manager->persist($cours);
        }




        // * Envoyer en base de donnée.
        $manager->flush();
    }
}
