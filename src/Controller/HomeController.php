<?php

namespace App\Controller;

use App\Entity\Cours;
use App\Entity\Categorie;
use App\Repository\CoursRepository;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    private $repoCategories;
    private $repoCours;

    function __construct(CategorieRepository $repoCategories, CoursRepository $repoCours)
    {
        $this->repoCategories = $repoCategories;
        $this->repoCours      = $repoCours;
    }

    // Afficher les cours compartimenter par catégorie
    #[Route('/', name: 'app_home')]
    #[Route('/cours', name: 'app_cours')]
    public function index(): Response
    {
        $courses = $this->repoCours->findAll();
        $categories = $this->repoCategories->findAll();

        return $this->render('home/index.html.twig', compact('courses'));
    }

    // Voir tous les cours sans aucun filtre
    #[Route('/tous-les-cours', name: 'app_all_courses')]
    public function showAll(CoursRepository $coursRepository): Response
    {
        $courses = $coursRepository->findAll();
        return $this->render('cours/all-courses.html.twig', compact('courses'));
    }
    
    // Lire un cours
    #[Route('/cours/{id<[0-9]+>}', methods: 'GET', name: 'app_cours_show')]
    public function show(Cours $cours): Response
    {
        return $this->render('cours/show.html.twig', compact('cours'));
    }

}
